var express = require('express'), 
    ntlm = require('express-ntlm');
var fs = require('fs'); 
const bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/getjson', function (req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Content-Type', 'application/json');
  try {  
    var data = fs.readFileSync('ServerTestJsonEighteenK.txt', 'utf8');
    res.send(data.toString());    
  } catch(e) {
    console.log('Error:', e.stack);
  }
	console.log("JSON is sent as response!!")

});

app.post('/postjson', function (request, response) {
  response.header("Access-Control-Allow-Origin", "*");

  let buffer = '';
  request.on('data', chunk => {
    buffer += chunk;
  });

  request.on('end', () => {

    const responseString = `Received string ${buffer}`;
    console.log(`Responding with: ${responseString}`);
    response.writeHead(200, "Content-Type: text/plain");
    // response.end(responseString);
    response.end("Success")
  });

}); 


app.listen(9000);