from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO
import json
import time

print("hello")
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

	def do_GET(self):
		request_path = self.path
		print("\n----- Request Start ----->\n")
		print("Requester IP Address :",self.client_address)
		print("request_path :", request_path)
		print("self.headers :", self.headers)
		print("<----- Request End -----\n")

		self.send_response(200,"SUCCESS")
		self.send_header('Access-Control-Allow-Origin', '*')
		self.send_header('Content-Type', 'application/json')
		self.end_headers()
		start = time.time()
		f = open("ServerTestJsonEighteenK.txt", "r")
		json_string = f.read()
		end = time.time()
		print(end - start)
		self.wfile.write(json_string.encode("utf-8"))

	def do_OPTIONS(self):
		self.send_response(200,"OK")
		# self.send_header('Access-Control-Allow-Credentials', 'true')
		self.send_header('Access-Control-Allow-Origin', '*')
		self.send_header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST')
		self.send_header('Access-Control-Allow-Headers', 'X-Requested-With')
		self.send_header('Access-Control-Allow-Headers', 'Content-Type')
		self.end_headers()
		
	def do_POST(self):
		content_length = int(self.headers['Content-Length'])
		body=self.rfile.read(content_length)
		self.send_response(200,"SUCCESS")
		self.send_header('Access-Control-Allow-Origin', '*')
		self.send_header('Content-Type', 'application/json')
		self.end_headers()
		response = BytesIO()
		response.write(b'Received Request: ')
		response.write(body)
		# self.wfile.write(response.getvalue())
		self.wfile.write("Success".encode("utf-8"))
		print(response.getvalue())
		
        

http=HTTPServer(('localhost', 8000), SimpleHTTPRequestHandler)
http.serve_forever()