
Office.onReady(info => {
  if (info.host === Office.HostType.Excel) {
    if (!Office.context.requirements.isSetSupported('ExcelApi', '1.7')) {
      console.log('Sorry. The tutorial add-in uses Excel.js APIs that are not available in your version of Office.');
  }
  //Called when Button is clicked.
  document.getElementById("apicall").onclick = apiTest; //To call Api POST method which will send the json to python server.
  document.getElementById("apicallGet").onclick = apicallGet; //To call Api GET method which will get a json response from python server
  
  $('.tabs-stage div').hide();
  $('.tabs-stage div:first').show();
  $('.tabs-nav li:first').addClass('tab-active');
  
  /* To check with the order of the tabs if wrong uncomment this section. */

  // $( document ).ready(function() {
  //   var $val = $('.tabs-nav a').html();
  //   run($val);  
  // });

  /* Jquery on click function to open tabs by their respective names */
  $('.tabs-nav a').on('click', function(event){
    event.preventDefault();
    $('.tabs-nav li').removeClass('tab-active');
    $(this).parent().addClass('tab-active');
    $('.tabs-stage div').hide();
    $($(this).attr('href')).show();
    var $val = $(this).html();
    /* If Tab name is employee do not execute the DataTable function. */
    if(!($val === "Employee")){
      /* To execute the DataTable function by calling the function run() .*/
      run($val);
    }
  });
    document.getElementById("sideload-msg").style.display = "none";
    document.getElementById("app-body").style.display = "flex";
  }
});

/* To save the data entered in the HTML form.*/
const form = document.forms['save-data']
form.addEventListener('submit', e => {
    e.preventDefault();
    /* Serializing the form data to array. */
    var data = $('form').serializeArray();
    /* getRowLength method is to get the length of the row where the form data is passed, which is also a nested function */ 
    getRowLength(data);
    form.reset();
  });    

  /* Method to get the row length of the sheet so that a new data can be added on the next available row number */
async function getRowLength(data){
  var globalLastRow:any = 0;
  const lastRowNumber = await Excel.run(function (ctx) {
    var sheet = ctx.workbook.worksheets.getItem("Employee Details")
    var uRange = sheet.getUsedRange();
    uRange.load(['rowCount']);
    return ctx
      .sync()
      .then(function () {
        globalLastRow = uRange.rowCount;
        /* Method that passes the serialized data with the row length of "Employee Details" Sheet */
        setData(data,globalLastRow);
      }).catch(function (error) {
        console.log(error);
      });
      
  }).catch(function (error) {
    console.log(error);
  });
}

/* Set Data function to set the form data in a sheet. */
function setData(data,rowSize){
  Excel.run(function (context) {
    /* A unique Employee ID is generated based on the row length */
    var empId = "Emp00"+(rowSize+1);
    /* The finalEmpData gets the serialized data to Array */
    var finalEmpData = data.map(function (obj) {
      return obj.value;
    });
    /* Inserting the Employee ID to array in the index position 0 */
    finalEmpData.unshift(empId) ;
    /* Setting the Form array to values which sets the data on available row based on the length of the row. */
    var values =[finalEmpData];
    var sheet = context.workbook.worksheets.getItem("Employee Details");

    var range = sheet.getRange("A"+(rowSize+1)).getResizedRange(values.length - 1, values[0].length - 1);
    range.values = values;

    return context.sync()
        .then(function () {
            console.log("Finished saving the data to Sheet.");
        });
  }).catch(function (error) {
      console.log(error);
  });
}

/* The run function gets called each time the tabs are changed */
/* The run function gets the total row and column which generates a range to get the data of sheet */
function run(sheetName){
  Excel.run(function (ctx) {
    var globalLastRow = 0;  
    var globalLastCol = 0;  
    var sheet = ctx.workbook.worksheets.getItem(sheetName)
    var uRange = sheet.getUsedRange();
    uRange.load(['rowCount', 'columnCount']);
    return ctx
      .sync()
      .then(function () {
        globalLastRow = uRange.rowCount;
        globalLastCol = uRange.columnCount;  
        /* Generates the Range based on row length and column length  {FOR TABS PURPOSE}*/
        var range = "A1:"+(globalLastCol + 9).toString(36).toUpperCase()+globalLastRow;
        /* GenJson function generates the json based on the range of a sheet {FOR TABS PURPOSE} */
        genJson(sheetName,range);
      }).catch(function (error) {
        console.log(error);
      });
  }).catch(function (error) {
    console.log(error);
  });
}
/* GenJson function generates the JSON for a sheet and with the range {FOR TABS PURPOSE}*/
function genJson(sheetName,range){
  Excel.run(function (ctx) {
    var sheet = ctx.workbook.worksheets.getItem(sheetName)
    var jsonData = sheet.getRange(range);
    jsonData.load("values");
    return ctx
      .sync()
      .then(function () {
        /* GenDataTable function generates a DataTable for sheets that are selected in TABS */
        genDataTable(sheetName,jsonData.values);
      }).catch(function (error) {
        console.log(error);
      });
  }).catch(function (error) {
    console.log(error);
  });
}

/* Generates DataTable for the TAB selected */
function genDataTable(sheetName,data){
  var tableHeader = data[0];
  var columnArr = []
  tableHeader = tableHeader.filter(function(entry) { return entry.trim() != ''; });
  /* Converting first row of range data to DataTable Header */
  for(var i=0; i<tableHeader.length;i++){
     var dict = { title: tableHeader[i] }
     columnArr[i] = dict
  }
  /* Removing the first row since it is already added to header. */
  data.splice(0,1)
  /* Converting any type of object to DataTable's requirement array */
  const firstKey = Object.keys(data)[1];
  if(!(data.length > 0)){
    var dataSet = data[firstKey];
  }else{
    var dataSet = data;
  }
  var columns = [];
  /** Mapping the data to Array of Objects for DataTable row */
  $.each( dataSet[0], function( key, value ) {
      
      var my_item = <any>{};
      my_item.data = key;
      my_item.title = key;
      columns.push(my_item);
  });

  /** Note: The DataTable id should be "example-{Name of the sheet where DataTable to be generated in HTML Page}" */
  /** Datatable is generated based on the table id, 
                    * where {{data: dataSet} is the Row data} and {{"columns": columnArr} is the Header}  */
  var table = $('#example-'+sheetName).DataTable({
    data: dataSet,
    "columns": columnArr
  });  
  /** destroys the Table when the TAB is reopened so that the data is loaded again */
  table.destroy();
}

/** Called when api call post button clicked */
function apiTest(){
  Excel.run(function(context) {
    /** Retrieves total worksheet in workbook */
    var sheets = context.workbook.worksheets;
    sheets.load("items/name");

    return context.sync().then(function() {
      /** Calling each sheet in function getRanges to get json for respective sheets */
      sheets.items.forEach(function(sheet) {
        getRanges(sheet.name)
      });
    });
  });
}

/** To get range data of each sheet */
function getRanges(sheetName){
  Excel.run(function (ctx) {
    var globalLastRow = 0;  
    var globalLastCol = 0;  
    var sheet = ctx.workbook.worksheets.getItem(sheetName)
    var uRange = sheet.getUsedRange();
    uRange.load(['rowCount', 'columnCount']);
    return ctx
      .sync()
      .then(function () {
        globalLastRow = uRange.rowCount;
        globalLastCol = uRange.columnCount;  
        var range = "A1:"+(globalLastCol + 9).toString(36).toUpperCase()+globalLastRow;
        /** Based on sheet name and range json is generated */
        apicallDataSet(sheetName,range)
      }).catch(function (error) {
        console.log(error);
      });
  }).catch(function (error) {
    console.log(error);
  });
}

/** Genrates JSON and passes to post method to send it to Python */
function apicallDataSet(sheetNames,ranges){
  Excel.run(function (ctx) {
      var sheetMovies = ctx.workbook.worksheets.getItem(sheetNames)
      var jsonData = sheetMovies.getRange(ranges);
      jsonData.load("values");
      return ctx
        .sync()
        .then(function () {
          /**JSON data to api post method */
          apicall(jsonData.values);
        }).catch(function (error) {
          console.log(error);
        });
      
  }).catch(function (error) {
    console.log(error);
  });
}

/** With the JSON the data is passed to Python */
function apicall(dataSetForApi) {

    fetch("http://localhost:8000/", {
      method: 'POST', 
      body:JSON.stringify(dataSetForApi) 
    }).then((response) => {
      return response.text().then(function(text) {
        console.log(text)
      })
    });
}

/** Called when api call get button is clicked */
async function apicallGet(){
  fetch("http://localhost:8000/",{
    method:"GET"  
  })
  .then((response) => {
      console.log(response)
      return response.text().then(function(data) {
        /** Calls the createTable function by passing response string */
        createTableFromResponse(data)
      });
  });
}

/** Function to parse response String and create a Table  */
function createTableFromResponse(data){
  var json = JSON.parse(data);

  var header = json[0];
    var tableData = [];
    json.shift();

    Excel.run(function (context) {
      var sheet = context.workbook.worksheets.getItem("Json To Sheet");
      /** Getting Range for table header by getting the length of header Array */
      var range = "A1:"+(header.length + 9).toString(36).toUpperCase()+"1"
      var tableName = "EmployeeTable";
      /** Deletes the table if available in order to replace with new set of data into table */
      var table = context.workbook.tables.getItemOrNullObject(tableName);
      table.delete();
  
      var expensesTable = sheet.tables.add(range, true );
      expensesTable.name = tableName;
      /** creates the table header where [header] is the header Array */
      expensesTable.getHeaderRowRange().values = [header];
      /** Table rows where "json" is the table row data */
      expensesTable.rows.add(null , json);
  
      if (Office.context.requirements.isSetSupported("ExcelApi", "1.2")) {
          sheet.getUsedRange().format.autofitColumns();
          sheet.getUsedRange().format.autofitRows();
      }
  
      sheet.activate();
  
      return context.sync();
  }).catch(function (error) {
    console.log(error);
  });
  
}
